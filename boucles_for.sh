for i in "$@"
do
 echo "$i"
done

for i in *
do
 echo $i
done

for i in `seq 1 10`
do
 echo $i
done

echo "deuxiéme syntaxe"

for ((i=0 ; 10 - $i ; i++))
    do echo $i
done
